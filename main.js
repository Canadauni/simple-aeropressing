fetch('coffees.json')
  .then((res) => {
    return res.json()
  })
  .then((coffees) => {
    coffeeNode = document.getElementById('coffee')
    coffeeNode.textContent = coffees[0].coffee + "g of coffee"
    waterNode = document.getElementById('water')
    waterNode.textContent = coffees[0].water + "g of water"
    grindNode = document.getElementById('grind')
    grindNode.textContent = coffees[0].grind
    invertedNode = document.getElementById('inverted')
    invertedNode.textContent = coffees[0].inverted ? "inverted" : "regular"
    timeNode = document.getElementById('time')
    timeNode.textContent = coffees[0].time
    notesNode = document.getElementById('notes')
    notesNode.textContent = coffees[0].notes
  })